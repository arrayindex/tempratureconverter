package com.confusedpenguins.tempratureconverer.domain.usecase.temprature

/**
 * Created by andrew on 12/28/17.
 */

enum class TemperatureScale {
    CELSIUS, FAHRENHEIT, KELVIN
}