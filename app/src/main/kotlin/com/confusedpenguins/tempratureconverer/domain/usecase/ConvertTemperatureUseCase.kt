package com.confusedpenguins.Temperatureconverer.domain.usecase

import com.confusedpenguins.tempratureconverer.domain.usecase.UseCase
import com.confusedpenguins.tempratureconverer.domain.usecase.temprature.TemperatureScale

/**
 * Created by andrew on 12/28/17.
 */

class ConvertTemperatureUseCase(val fromTemperatureScale: TemperatureScale, val toTemperatureScale: TemperatureScale, val temperature: Double) : UseCase<Double> {

    override fun execute(): Double {
        return when (fromTemperatureScale) {
            TemperatureScale.FAHRENHEIT -> convertFromFahrenheit()
            TemperatureScale.KELVIN -> convertFromKelvin()
            TemperatureScale.CELSIUS -> convertFromCelsius()
        }
    }

    private fun convertFromCelsius(): Double {
        return when (toTemperatureScale) {
            TemperatureScale.CELSIUS -> temperature
            TemperatureScale.KELVIN -> convertCelsiusToKelvin()
            TemperatureScale.FAHRENHEIT -> convertCelsiusToFahrenheit()
        }
    }

    private fun convertFromKelvin(): Double {
        return when (toTemperatureScale) {
            TemperatureScale.CELSIUS -> convertKelvinToCelsius()
            TemperatureScale.KELVIN -> temperature
            TemperatureScale.FAHRENHEIT -> convertKelvinToFahrenheit()
        }
    }

    private fun convertFromFahrenheit(): Double {
        return when (toTemperatureScale) {
            TemperatureScale.CELSIUS -> convertFahrenheitToCelsius()
            TemperatureScale.KELVIN -> convertFahrenheitToKelvin()
            TemperatureScale.FAHRENHEIT -> temperature
        }
    }

    private fun convertCelsiusToFahrenheit(): Double {
        return (1.8 * temperature) + 32
    }

    private fun convertCelsiusToKelvin(): Double {
        return temperature + 273.15
    }

    private fun convertFahrenheitToCelsius(): Double {
        return (temperature - 32.0) / 1.8
    }

    private fun convertFahrenheitToKelvin(): Double {
        return (temperature + 459.679) * (5.0 / 9.0)
    }

    private fun convertKelvinToCelsius(): Double {
        return temperature - 273.15
    }

    private fun convertKelvinToFahrenheit(): Double {
        return (temperature * (9.0 / 5.0)) - 459.679
    }
}