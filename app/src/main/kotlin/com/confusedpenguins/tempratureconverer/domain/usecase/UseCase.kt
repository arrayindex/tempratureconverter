package com.confusedpenguins.tempratureconverer.domain.usecase

/**
 * Created by andrew on 12/28/17.
 */
interface UseCase<out E> {
    fun execute(): E
}