package com.confusedpenguins.tempratureconverer.ui

import android.app.Application
import com.confusedpenguins.tempratureconverer.extensions.DelegatesExt

/**
 * Created by andrew on 12/28/17.
 */

class App : Application() {

    companion object {
        var INSTANCE: App by DelegatesExt.notNullSingleValue()
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }
}