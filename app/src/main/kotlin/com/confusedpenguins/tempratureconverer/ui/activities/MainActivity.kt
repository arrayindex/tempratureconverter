package com.confusedpenguins.tempratureconverer.ui.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.confusedpenguins.tempratureconverer.R

/**
 * Created by andrew on 12/28/17.
 */

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
    }
}