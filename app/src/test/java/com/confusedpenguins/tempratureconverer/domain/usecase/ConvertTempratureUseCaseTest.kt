package com.confusedpenguins.tempratureconverer.domain.usecase

import com.confusedpenguins.Temperatureconverer.domain.usecase.ConvertTemperatureUseCase
import com.confusedpenguins.tempratureconverer.domain.usecase.temprature.TemperatureScale
import org.junit.Assert
import org.junit.Test

/**
 * Created by andrew on 12/28/17.
 */

class ConvertTemperatureUseCaseTest {
    @Test
    fun addition_isCorrect() {
        Assert.assertEquals(4, 2 + 2)
    }

    @Test
    fun when_celsius_is_passed_all_calculations_are_correct() {
        var toFahrenheitUseCase = ConvertTemperatureUseCase(TemperatureScale.CELSIUS, TemperatureScale.FAHRENHEIT, 0.00)
        var toKelvinUseCase = ConvertTemperatureUseCase(TemperatureScale.CELSIUS, TemperatureScale.KELVIN, 0.00)
        var toCelsiusUseCase = ConvertTemperatureUseCase(TemperatureScale.CELSIUS, TemperatureScale.CELSIUS, 0.00)

        Assert.assertEquals(273.15, toKelvinUseCase.execute(), 0.0)
        Assert.assertEquals(32.0, toFahrenheitUseCase.execute(), 0.0)
        Assert.assertEquals(0.0, toCelsiusUseCase.execute(), 0.0)
    }

    @Test
    fun when_fahrenheit_is_passed_all_calculations_are_correct() {
        var toFahrenheitUseCase = ConvertTemperatureUseCase(TemperatureScale.FAHRENHEIT, TemperatureScale.FAHRENHEIT, 0.00)
        var toKelvinUseCase = ConvertTemperatureUseCase(TemperatureScale.FAHRENHEIT, TemperatureScale.KELVIN, 0.00)
        var toCelsiusUseCase = ConvertTemperatureUseCase(TemperatureScale.FAHRENHEIT, TemperatureScale.CELSIUS, 0.00)

        Assert.assertEquals(255.37, toKelvinUseCase.execute(), 1.0)
        Assert.assertEquals(0.0, toFahrenheitUseCase.execute(), 1.0)
        Assert.assertEquals(-17.78, toCelsiusUseCase.execute(), 1.0)
    }

    @Test
    fun when_kelvin_is_passed_all_calculations_are_correct() {
        var toFahrenheitUseCase = ConvertTemperatureUseCase(TemperatureScale.KELVIN, TemperatureScale.FAHRENHEIT, 0.00)
        var toKelvinUseCase = ConvertTemperatureUseCase(TemperatureScale.KELVIN, TemperatureScale.KELVIN, 0.00)
        var toCelsiusUseCase = ConvertTemperatureUseCase(TemperatureScale.KELVIN, TemperatureScale.CELSIUS, 0.00)

        Assert.assertEquals(0.0, toKelvinUseCase.execute(), 1.0)
        Assert.assertEquals(-459.67, toFahrenheitUseCase.execute(), 1.0)
        Assert.assertEquals(-273.15, toCelsiusUseCase.execute(), 1.0)
    }
}